#ifndef FILE_DEF
#define FILE_DEF

#include <stdio.h>


FILE* open_file(char *name, char *mode);
int close_file(FILE *file);
#endif
