#ifndef IMAGE_DEF
#define IMAGE_DEF
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width;
    size_t height;
    struct pixel* pixels;
};


size_t get_height(struct image* image);
void set_height(struct image* image, size_t height);
size_t get_width(struct image* image);
void set_width(struct image* image, size_t width);
struct pixel* get_pixel_array(struct image* image);
struct pixel get_pixel(struct image* image, size_t x_axis, size_t y_axis);
void set_pixel_array(struct image* image, struct pixel* pixels);
void set_pixel(struct image* image, struct pixel pixel, size_t x_axis, size_t y_axis);
struct image* rotate_image(struct image* original_image);
struct image* alloc_image(size_t width, size_t height);
struct pixel* alloc_pixels(size_t width, size_t height);
void free_image(struct image* image);
void free_pixel_array(struct pixel* pixels);
#endif

