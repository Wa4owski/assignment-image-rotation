#include "bmp_util/bmp_util.h"
#include "file/file.h"
#include "img/image.h"
#include <stdio.h>



int main( int argc, char** argv ) {
    (void) argc; (void) argv; // suppress 'unused parameters' warning

    if (argc < 3) {
        fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>");
        return -1;
    }

    FILE* original_bmp = open_file(argv[1], "r");
    if (!original_bmp){
        fprintf(stderr, "Can't open original bmp file\n");
        return -1;
    }
    
    FILE* new_bmp = open_file(argv[2], "w");
    if (!new_bmp){
        fprintf(stderr, "Can't open target bmp file\n");
        return -1;
    }
    
    struct image* original_image = alloc_image(0, 0);
    enum read_status convert_status = from_bmp(original_bmp, original_image);
    if (convert_status != READ_OK){
        fprintf(stderr, "Converting from bmp to image struct has been failed\n");
	free_image(original_image);
        return -1;
    }
    close_file(original_bmp);

    struct image* rotated_image = rotate_image(original_image);
    if (!rotated_image){
        fprintf(stderr, "Rotation was failed!\n");
        free_image(original_image);
	return -1;
    }

    enum write_status write_result = to_bmp(new_bmp, rotated_image);
    if (write_result != WRITE_OK){
        fprintf(stderr, "Converting image structe to bmp has been failed\n");
	free_image(original_image);
   	free_image(rotated_image);        
	return -1;
    }

    close_file(new_bmp);
    printf("debug");
    free_image(original_image);
    free_image(rotated_image);
    return 0;
}
