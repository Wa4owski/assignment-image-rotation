#include "bmp_util.h"


#define DWORD_SIZE 4
#define COLOR_DEPTH 24
#define BMP_TYPE 0x4D42
#define BMP_RESERVED 0
#define BMP_COMPRESSION 0
#define BMP_PIXEL_PER_METER 2834
#define BMP_PLANES 1
#define BMP_COLORS 0
#define BMP_HEADER_SIZE 40
#define DWORD_SIZE 4
#define COLOR_DEPTH 24

struct bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
} __attribute__((packed));

enum read_status from_bmp(FILE* in, struct image* image) {
    if (!in) return READ_FAIL_NULL_PTR;
    struct bmp_header header = {0};
    enum read_status header_read_status =  read_bmp_header(in, &header);
    if (header_read_status != READ_OK) return header_read_status;
    print_bmp_header(&header);
    set_height(image, header.biHeight);
    set_width(image, header.biWidth);
    return read_bmp_pixel_array(in, image, &header);
}


enum write_status to_bmp(FILE *out, struct image *image) {
    if (!out || !image) {
        return WRITE_ERROR;
    }

    struct bmp_header header = {0};
    init_bmp_header(&header, image);

    size_t image_width = get_width(image);
    size_t image_height = get_height(image);
    uint8_t padding = get_padding(image_width);

    struct pixel *pixel_array = get_pixel_array(image);

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    fseek(out, header.bOffBits, SEEK_SET);

    uint8_t *padding_trash = calloc(1, padding);

    for (size_t i = 0; i < image_height; i++) {
        fwrite(pixel_array + header.biWidth * i, header.biWidth * sizeof(struct pixel), 1, out);
        fwrite(padding_trash, padding, 1, out);
    }

    free(padding_trash);

    return WRITE_OK;
}



void init_bmp_header(struct bmp_header* header, struct image* image) {
    size_t image_width = get_width(image);
    size_t image_height = get_height(image);
    uint8_t padding = get_padding(image_width);

    size_t image_size = (sizeof(struct pixel) * image_width + padding) * image_height;

    header->bfType = BMP_TYPE;
    header->biBitCount = COLOR_DEPTH;
    header->biXPelsPerMeter = BMP_PIXEL_PER_METER;
    header->biYPelsPerMeter = BMP_PIXEL_PER_METER;
    header->bfileSize = image_size + BMP_RESERVED + sizeof(struct bmp_header);
    header->bfReserved = BMP_RESERVED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BMP_HEADER_SIZE;
    header->biWidth = image_width;
    header->biHeight = image_height;
    header->biPlanes = BMP_COMPRESSION;
    header->biCompression = BMP_COMPRESSION;
    header->biSizeImage = image_size;
    header->biClrUsed = BMP_COLORS;
    header->biClrImportant = BMP_COLORS;
}


void print_bmp_header(struct bmp_header* header) {
    printf("BMP TYPE: %hu\n", header->bfType);
    printf("FILE SIZE: %u\n", header->bfileSize);
    printf("RESERVED: %u\n", header->bfReserved);
    printf("OFF BITS: %u\n", header->bOffBits);
    printf("SIZE: %u\n", header->biSize);
    printf("WIDTH: %u\n", header->biWidth);
    printf("HEIGHT: %u\n", header->biHeight);
    printf("PLANES: %hu\n", header->biPlanes);
    printf("BIT COUNT: %hu\n", header->biBitCount);
    printf("COMPRESSION: %u\n", header->biCompression);
    printf("SIZE IMAGE: %u\n", header->biSizeImage);
    printf("PIXELS PER METER X: %u\n", header->biXPelsPerMeter);
    printf("PIXELS PER METER Y: %u\n", header->biYPelsPerMeter);
    printf("CLR USED: %u\n", header->biClrUsed);
    printf("CLR IMPORTANT: %u\n", header->biClrImportant);

}


enum read_status read_bmp_header(FILE* in, struct bmp_header* header) {
    rewind(in);
    fread(header, sizeof(struct bmp_header), 1, in);
    return READ_OK;
}


enum read_status read_bmp_pixel_array(FILE* in, struct image* image, struct bmp_header* header) {
    size_t img_width = header->biWidth;
    size_t img_height = header->biHeight;
    struct pixel* pixel_array = alloc_pixels(img_width, img_height);
    if (!pixel_array) return READ_FAIL_NULL_PTR;
    rewind(in);
    fseek(in, header->bOffBits, SEEK_SET);
    uint8_t padding = get_padding(img_width);
    for (size_t i = 0; i < img_height; i++) {
        fread(pixel_array + i * img_width, sizeof (struct pixel) * img_width, 1 , in);
        fseek(in, padding, SEEK_CUR);
    }
    free_pixel_array(get_pixel_array(image));
    set_pixel_array(image, pixel_array);
    return READ_OK;
}


uint8_t get_padding(size_t width){
    return DWORD_SIZE - (width * (COLOR_DEPTH / 8)) % DWORD_SIZE;
}
