#ifndef BMPH
#define BMPH
#include "../img/image.h"
#include <stdint.h>
#include <stdio.h>

struct bmp_header;

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FAIL_NULL_PTR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp( FILE* in, struct image* image );
enum write_status to_bmp( FILE* out, struct image* image );
enum read_status read_bmp_header(FILE* in, struct bmp_header* header);
enum read_status read_bmp_pixel_array(FILE* in, struct image* image, struct bmp_header* header);
void init_bmp_header(struct bmp_header* header, struct image* image);
void print_bmp_header(struct bmp_header* header);
uint8_t get_padding(size_t width);
#endif
